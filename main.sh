#!/usr/bin/env bash

set -e

# ############### #
# Default Globals #
# ############### #

APPLICATION_NAME="rpi-coreos-installer"
FCOS_CONFIG_PATH="$1"
FCOS_DISK=NOT_DEFINED_DEFAULT
PI_FIRMWARE_VERSION=NOT_DEFINED_DEFAULT

# ######### #
# Functions #
# ######### #

function check_program() {
  for program in "lsblk" "jq" "podman" "unzip" "curl" "sudo"; do
    if ! type ${program} >/dev/null; then
      echo >&2 "${program} is needed for this script!"
      exit 1
    fi
  done
}

function process_arguments() {
  if [[ -z "$FCOS_CONFIG_PATH" ]]; then
    echo "FCOS_CONFIG_PATH variable is empty. The first argument must be a path to a valid ignition configuration." 1>&2
    exit 1
  fi
}

function fetch_disk() {
  if type fzf >/dev/null; then
    local ptuuid_mounted_disks
    ptuuid_mounted_disks=$(lsblk --noheadings --raw -o PTUUID,MOUNTPOINT | grep -E "[^\n] .*/." | awk -F' ' '{print $1}' | uniq | xargs | tr ' ' '|')

    FCOS_DISK=$(lsblk -dn -o PATH,TRAN,SIZE,PTUUID | grep -Ev "$ptuuid_mounted_disks" | fzf | awk -F' ' '{print $1}')
  fi
}

function compile_butane() {
  if [ "${FCOS_CONFIG_PATH: -4}" == ".yml" ] || [ "${FCOS_CONFIG_PATH: -5}" == ".yaml" ] || [ "${FCOS_CONFIG_PATH: -3}" == ".bu" ]; then
    echo "Compile butane code $FCOS_CONFIG_PATH"
    local processed_fcos_config_path=/tmp/transpiled_config.ign
    podman run -i --rm quay.io/coreos/butane:release --pretty --strict <"$FCOS_CONFIG_PATH" >"$processed_fcos_config_path"
    FCOS_CONFIG_PATH=$processed_fcos_config_path
  fi
}

function install_fcos() {
  echo "Install FCOS on $FCOS_DISK with $FCOS_CONFIG_PATH"
  sudo podman run --pull=always --privileged --rm -v /dev:/dev -v /run/udev:/run/udev -v "$PWD":/data -v "$FCOS_CONFIG_PATH:/data/igntion.ign" -w /data quay.io/coreos/coreos-installer:release install --architecture=aarch64 -i /data/igntion.ign "$FCOS_DISK"
  sync
}

function install_uefi() {
  echo "Install UEFI on $FCOS_DISK ($fcos_partition)"

  local fcos_partition
  fcos_partition=$(lsblk "$FCOS_DISK" -J -o LABEL,PATH | jq -r '.blockdevices[] | select(.label == "EFI-SYSTEM")'.path)

  mkdir -p /tmp/FCOSEFIpart
  sudo mount "$fcos_partition" /tmp/FCOSEFIpart
  pushd /tmp/FCOSEFIpart

  if [[ "$PI_FIRMWARE_VERSION" == "NOT_DEFINED_DEFAULT" ]]; then
    PI_FIRMWARE_VERSION=$(curl -s 'https://github.com/pftf/RPi4/releases' | grep -Po "(?<=>)(v[0-9]{1,2}.[0-9]{1,2})" | sort -r | head -n1)
  fi

  sudo curl -LO "https://github.com/pftf/RPi4/releases/download/${PI_FIRMWARE_VERSION}/RPi4_UEFI_Firmware_${PI_FIRMWARE_VERSION}.zip"
  sudo unzip "RPi4_UEFI_Firmware_${PI_FIRMWARE_VERSION}.zip"
  sudo rm "RPi4_UEFI_Firmware_${PI_FIRMWARE_VERSION}.zip"
  popd
  sync
  sudo umount /tmp/FCOSEFIpart
}

display_help() {
  echo "$APPLICATION_NAME"
  echo " "
  echo "$APPLICATION_NAME [options] [BUTANE_OR_IGNITION_FIlE_PATH]"
  echo " "
  echo "options:"
  echo "--rpi-firwmare-version    what rpi firmware version to embed in the ISO"
  echo "-v                        set debug mode"
  echo " "
  exit 0
}

process_arguments() {
  while test $# -gt 0; do
    case "$1" in
    -h | --help)
      display_help
      ;;
    -v)
      set -x
      shift
      ;;
    --rpi-firwmare-version)
      PI_FIRMWARE_VERSION=$1
      shift
      ;;
    *)
      print_error "\e[1m$1\e[22m is not a valid option."
      exit 1
      ;;
    esac
  done
}

# #### #
# Main #
# #### #

main() {
  process_arguments "$@"

  check_program

  fetch_disk

  read -p "Proceed with ‘$FCOS_DISK’. THIS WILL WIPE THE DISK. Are you sure? [y/N] " -n 1 -r
  echo
  if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    echo "Refuses by user. Terminating."
    exit 0
  fi

  compile_butane
  install_fcos
  install_uefi
}

main "$@"
