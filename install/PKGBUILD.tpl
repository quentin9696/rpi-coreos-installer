# Maintainer: Guillaume Donval <(firstname)(lastname)(at)firemail(dot)cc>

pkgname="rpo-coreos-installer"
pkgver="<#PKGVER>"
pkgrel=<#PKGREL>
pkgdesc="Installs CoreOS on a Raspberri Pi"
arch=("any")
url="https://gitlab.com/gui-don/$pkgname"
license=("GPL3")
depends=("lsblk", "podman", "jq>=1", "sudo", "unzip", "curl")
provides=("rpi-coreos-installer")
source=("https://gitlab.com/gui-don/$pkgname/-/archive/$pkgver/rpi-coreos-installer-$pkgver.tar.gz")
b2sums=("<#B2SUM>")

package() {
  cd "$pkgname-$pkgver"

  install/install.sh "$pkgdir" "$pkgname"
}
